class Router:

    modelName: str

    def __init__(self):
        self.modelName = "Default"
        print("Konstruktor wurde aufgerufen")

    def ersteMethode(self):
        print("Ich bin eine Methode")

    def zweiteMethode(self, name):
        #print("Ich gebe auch deinen Namen aus, {}".format(name))
        print("Ich gebe auch deinen Namen aus " + name)

    def setModelName(self, name):
        self.modelName = name

    def printModelName(self):
        print("Modelname: " + self.modelName)

    @staticmethod
    def statischeMethode():
        print("Ich bin eine statische Methode")


if __name__ == '__main__':
    print("Ich wurde direkt aufgerufen!")
    Router.statischeMethode()
    router = Router()
    router.ersteMethode()
    router.zweiteMethode("Timo")
    router.setModelName("ISR4321")
    router.printModelName()
