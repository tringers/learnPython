# LearnPython

Dies ist ein Tutorial, welches den Einstieg in Python ermöglichen soll.
Es wird versucht auf einfache Weise darzustellen, wie welche Komponenten 
funktionieren und wie diese miteinander arbeiten.

## Getting started

### IDE

Von der Seite https://www.jetbrains.com/pycharm/download/ könnt ihr euch die
entsprechende Version der Python Community Edition herunterladen. Diese steht
kostenlos zur Verfügung. Im Bildungsbereich gibt es eine Möglichkeit PyCharm
Professional kostenlos zu erhalten. Es steht natürlich jedem frei eine andere
IDE zu verwenden.

### Git

Wenn ihr Windows verwendet, könnt ihr Git Bash sowie Git GUI von der Seite
https://git-scm.com/download/win herunterladen. Unter Linux (hier am Beispiel
von Debian basierten Linux Distributionen) könnt ihr git einfach mit dem Befehl
"apt-get install git" installieren.
Ist die Installation durchgeführt, kann das Repository einfach durch die Eingabe
von "git clone https://gitlab.com/tringers/learnPython.git" heruntergeladen
werden.